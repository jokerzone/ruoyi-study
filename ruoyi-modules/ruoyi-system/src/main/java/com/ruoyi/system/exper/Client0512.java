package com.ruoyi.system.exper;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Client0512 {


    public static void main(String[] args) {


    }

    // 给定一个字符串 s ，请你找出其中不含有重复字符的 最长子串 的长度。

    public static int test(String s) {
        // 找出关系
        int length = s.length();
        int[] dp = new int[length + 1];
        HashMap<Character, Integer> characterIntegerHashMap = new HashMap<>();
        for (int i = 0; i < length; i++) {
            // 包含时，找到对应的位置
            if (!characterIntegerHashMap.containsKey(s.charAt(i))) {
                characterIntegerHashMap.put(s.charAt(i), i);
                dp[i + 1] = dp[i] + 1;
            } else {
                // 包含
                dp[i + 1] = i - characterIntegerHashMap.get(s.charAt(i));
                characterIntegerHashMap.put(s.charAt(i), i);
            }
        }
        Arrays.sort(dp);
        return dp[s.length()];

    }


    public static int small(int[][] arr) {

        int m = arr.length;
        int n = arr[0].length;
        int[][] dp = new int[m][n];
        dp[0][0] = arr[0][0];
        for (int i = 1; i < m; i++) {
            dp[i][0] = dp[i - 1][0] + arr[i][0];
        }
        for (int h = 1; h < n; h++) {
            dp[0][h] = dp[0][h - 1] + arr[0][h];

        }
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                dp[i][j] = Math.min(dp[i - 1][j], dp[i][j - 1]) + arr[i][j];
            }
        }

        return dp[m][n];

    }


    public static int test1(String s) {

        int length = s.length();
        int left = 0;
        int right = 0;
        int maxlength = 0;
        HashMap<Character, Integer> characterIntegerHashMap = new HashMap<>();
        while (right < length) {
            //
            char c = s.charAt(right);
            if (!characterIntegerHashMap.containsKey(c)) {
                // 包含了，移动左指针
                Integer integer = characterIntegerHashMap.get(c);
                left = integer;
                characterIntegerHashMap.put(c, right);
            } else {
                characterIntegerHashMap.put(s.charAt(right), right);
            }
            maxlength = Math.max(maxlength, right - left);
            right++;

        }


        return maxlength;
    }


    //给你一个数组 seats 表示一排座位，其中 seats[i] = 1 代表有人坐在第 i 个座位上，seats[i] = 0 代表座位 i 上是空的（下标从 0 开始）。
    //
    //至少有一个空座位，且至少有一人已经坐在座位上。
    //
    //亚历克斯希望坐在一个能够使他与离他最近的人之间的距离达到最大化的座位上。
    //
    //返回他到离他最近的人的最大距离。
    public int maxDistToClosest(int[] seats) {

        int left = 0;
        int length = seats.length;
        int max = 0;
        int start = 0;
        for (int i = 0; i < length; i++) {
            if (seats[i] == 1) {
                if (start == 0) {
                    start = i;
                }
                max = Math.max(max, (i - left) / 2);
                left = i;
            }
        }

        if (start > 0) {
            // 左边存在全是0 的情况
            max = Math.max(max, start);
        }

        if (left < length - 1) {
            //右边存在 全是 0 的情况
            max = Math.max(max, length - 1 - left);

        }

        return max;
    }
}
