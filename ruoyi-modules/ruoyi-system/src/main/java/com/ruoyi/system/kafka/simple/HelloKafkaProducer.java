package com.ruoyi.system.kafka.simple;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.*;

public class HelloKafkaProducer {
    public static void main(String[] args) {

        Map<String, Object> configs = new HashMap<>();
        // 指定初始连接用到的broker地址
        configs.put("bootstrap.servers", "127.0.0.1:9092");
        // 指定key的序列化类
        configs.put("key.serializer", StringSerializer.class);
        // 指定value的序列化类
        configs.put("value.serializer", StringSerializer.class);

        KafkaProducer<String, String> producer = new KafkaProducer<String, String>(configs);

        // 用于设置用户自定义的消息头字段
        List<Header> headers = new ArrayList<>();
        headers.add(new RecordHeader("biz.name", "producer.demo".getBytes()));

        ProducerRecord<String, String> record = new ProducerRecord<String, String>(
                "topic_2",
                0,
                "test",
                "hello lagou 0",
                headers
        );

        // 消息的同步确认

        // 消息的异步确认
        producer.send(record, new Callback() {
            @Override
            public void onCompletion(RecordMetadata metadata, Exception exception) {

                if (exception == null) {
                    System.out.println("消息的主题：" + metadata.topic());
                    System.out.println("消息的分区号：" + metadata.partition());
                    System.out.println("消息的偏移量：" + metadata.offset());
                } else {
                    System.out.println("异常消息：" + exception.getMessage());
                }
            }
        });

        // 关闭生产者
        producer.close();


    }
}
