package com.ruoyi.system.spi;

import com.google.common.collect.ComparisonChain;

import java.lang.reflect.Array;
import java.util.*;

public class Client0505 {

    public static void main(String[] args) {


        int[] beginTime = {9, 0};
        int[] endtime = {16, 0};

        int[] arr1 = {1, 5, 7};
        int[] arr2 = {2, 8, 6};
        int[] arr3 = {3, 7, 4};

        int[][] ints = {arr1, arr2, arr3};
        int a = isArrival(beginTime, endtime, ints);
        System.out.println(a);
    }

    private static int isArrival(int[] begintime, int[] endtime, int[][] arrivals) {
        int beginminuts = 60 * begintime[0] + begintime[1];
        int endminuts = 60 * endtime[0] + endtime[1];

        ArrayList<Integer> integers = new ArrayList<>();
        int[] first = {18 * 60, 20 * 60};
        int[] second = {12 * 60, 14 * 60};
        int[] three = {8 * 60, 10 * 60};
        HashMap<Integer, ArrayList<int[]>> integerHashMap = new HashMap<>();
        //  深度遍历，遍历每一个数组
        for (int i = 0; i < arrivals.length; i++) {
            int expend = 0;
            int time = 0;
            // 人到之前新增的数据才算有用，人到之后新增的数据没用
            int[] currentArrival = arrivals[0];
            // 路径长度
            int roadLong = currentArrival[1];
            // 人数
            int people = currentArrival[2];
            int roadtime = roadLong * 10;
            int alltime = roadtime + beginminuts;
            if (alltime > 20 * 60) {
                continue;
            }
            // 分段算有多少人
            int firsttime = (alltime - first[0]) * 20;
            int beyondsecondtime = (alltime - (alltime - first[0]) - second[1]) / 5;
            // 第二段
            int secondtime = (alltime - (alltime - first[0]) - (alltime - (alltime - first[0]) - second[1]) - second[0]) * 10;

            // 第三段 10-12
            int beyondthreetime = (alltime - secondtime / 10 - beyondsecondtime / 5 - firsttime / 20 - three[1]) / 5;

            // 第四段 8-10
            int threetime = (alltime - secondtime / 10 - beyondsecondtime / 5 - firsttime / 20 - beyondthreetime * 5 - beginminuts) * 3;

            int masttime = firsttime + beyondsecondtime + secondtime + beyondthreetime + threetime;
            int maxtime = masttime + people + beginminuts;
            if (maxtime > endminuts) {
                continue;
            }
            expend = arrivals[i][1] * 10;
            int[] value = new int[4];

            for (int j = 0; j < arrivals[i].length; j++) {
                value[j] = arrivals[i][j];
            }
            value[3] = expend;

            if (integerHashMap.containsKey(maxtime)) {

            } else {
                ArrayList<int[]> intValues = new ArrayList<>();
                intValues.add(value);
                integerHashMap.put(maxtime, intValues);
                integers.add(masttime);
            }

        }

        //

        Collections.sort(integers);
        for (int i = 0; i < integers.size(); i++) {

            ArrayList<int[]> ints = integerHashMap.get(integers.get(i));
            ints.sort(new Comparator<int[]>() {
                @Override
                public int compare(int[] o1, int[] o2) {
                    return ComparisonChain.start().compare(o1[1], o2[1]).compare(o1[2], o2[2]).result();
                }
            });

            for (int k = 0; k < ints.size(); k++) {

                int[] ints1 = ints.get(k);
                int i1 = ints1[0];
                HashMap<Object, Object> objectObjectHashMap = new HashMap<>();

                for (int j = k + 1; j < ints.size(); j++) {


                }

            }
        }

        return integers.size();
    }


    public static int solution2(int[] height) {
        int left = 0;
        int right = height.length - 1;
        int lmax = height[0];
        int rmax = height[right];
        int res = 0;
        while (left < right) {
            if (lmax < rmax) {
                if (lmax > height[left]) {
                    res += lmax - height[left];
                } else {
                    lmax = height[left];
                }
                left++;
            } else {
                if (rmax > height[right]) {
                    res += rmax - height[right];

                } else {
                    rmax = height[right];
                }
                right--;
            }
        }

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("线程");
            }
        });
        try {
            thread.start();
            thread.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return res;
    }

}