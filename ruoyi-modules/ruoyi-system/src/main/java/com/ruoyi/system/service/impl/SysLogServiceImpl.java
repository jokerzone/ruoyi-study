package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.SysLog;
import com.ruoyi.system.mapper.SysLogMapper;
import com.ruoyi.system.service.ISysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysLogServiceImpl implements ISysLogService {


    @Autowired
    private SysLogMapper sysLogMapper;

    @Override
    public int insertLog(SysLog sysLog) {
        int i = sysLogMapper.insertLog(sysLog);
        return i;
    }

    @Override
    public SysLog selectSysLog(Long pkid) {
        return sysLogMapper.selectLog(pkid);
    }

    @Override
    public int updateSysLog(SysLog sysLog) {
        return sysLogMapper.updateSysLog(sysLog);
    }

    @Override
    public List<SysLog> getList(SysLog sysLog) {

        return sysLogMapper.getList(sysLog);
    }


    @Override
    public List<SysLog> getListByIds(Long[] pkids) {

        return sysLogMapper.getListByIds(pkids);
    }
}
