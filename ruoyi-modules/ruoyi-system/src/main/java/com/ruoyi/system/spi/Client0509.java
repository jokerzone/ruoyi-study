package com.ruoyi.system.spi;

import com.google.common.collect.ComparisonChain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class Client0509 {

    public static void main(String[] args) {


        Object ob = new Object();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ob.wait();
                    //   Thread.sleep(10000);
                    System.out.println("子线程执行完成");
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        });


        Thread threadb = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //   Thread.sleep(10000);
                    System.out.println("子线程执行完成B");
                    ob.notifyAll();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        });

        thread.start();
        threadb.start();

//        thread.start();
//        try {
//            thread.join();
//        } catch (InterruptedException e) {
//            throw new RuntimeException(e);
//        }

//        try {
//            ob.wait();
//        } catch (InterruptedException e) {
//            throw new RuntimeException(e);
//        }
        System.out.println("父线程执行");
    }

}