package com.ruoyi.system.exper;


import java.util.*;

public class Client0508 {


    public static void main(String[] args) {

        int numCourses = 2;
        int[][] ints = new int[2][2];
        int[] arr = new int[]{0, 1};
        int[] arr1 = new int[]{1, 0};
        ints[0] = arr;
        ints[1]=arr1;
        boolean b = canFinish(numCourses, ints);
        System.out.println(b);
    }


    public static boolean canFinish(int numCourses, int[][] prerequisites) {
        // 初始化
        int[] ints = new int[numCourses];
        for (int i = 0; i < ints.length; i++) {
            ints[i] = i;
        }
        for (int i = 0; i < prerequisites.length; i++) {
            if (!setPC(ints, prerequisites[i])) {
                return false;
            }
        }
        return true;
    }

    private static Boolean setPC(int[] ints, int[] prerequisite) {
        // 如果出现环装，直接返回false
        if (ints[prerequisite[1]] == ints[prerequisite[0]]) {
            return false;
        }
        int i = prerequisite[1];
        while (i != 0) {
            // 已经到顶层了
            if (ints[i] == i) {
                break;
            }
            i = ints[i];
        }
        ints[prerequisite[0]] = ints[i];

        return true;
    }


}
