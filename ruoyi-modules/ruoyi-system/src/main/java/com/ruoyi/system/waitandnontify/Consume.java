package com.ruoyi.system.waitandnontify;

import java.util.concurrent.TimeUnit;

public class Consume implements Runnable {

    private Object object;

    public Consume(Object object) {
        this.object = object;
    }

    @Override
    public void run() {
        synchronized (object) {
            System.out.println("---- 进入消费者线程");
            System.out.println("---- 当前产品数量:" + Product.getCount());
            // 判断条件是否满足（有没有产品可以消费），若不满足则等待
            while (true) {
                try {
                    TimeUnit.MILLISECONDS.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (Product.getCount() <= 0) {
                    try {
                        System.out.println("---- 没有产品，进入等待");
                        // 通知生产者生产，自己进入等待
                        object.notify();
                        object.wait();
                        System.out.println("---- 结束等待,开始消费");
                        Product.delete();
                        System.out.println("---- 消费后产品数量:" + Product.getCount());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    System.out.println("---- 已有产品，直接消费");
                    Product.delete();
                    System.out.println("---- 消费后产品数量:" + Product.getCount());
                }


            }

        }
    }
}