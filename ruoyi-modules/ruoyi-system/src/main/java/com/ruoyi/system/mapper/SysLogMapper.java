package com.ruoyi.system.mapper;


import com.ruoyi.system.domain.SysLog;

import java.util.List;

public interface SysLogMapper {


    /**
     * 更新日志
     *
     * @param sysLog 日志信息
     * @return 主键
     */
    public int updateSysLog(SysLog sysLog);


    /**
     * 保存日志
     *
     * @param sysLog 日志信息
     * @return 主键
     */
    public int insertLog(SysLog sysLog);


    /**
     * 保存日志
     *
     * @param pkid 主键
     * @return 主键
     */
    public SysLog selectLog(Long pkid);

    /**
     * 获取博客
     *
     * @return
     */
    public List<SysLog> getList(SysLog sysLog);

    /**
     * 通过ids 获取博文
     *
     * @return
     */
    public List<SysLog> getListByIds( Long[] pkids);

}
