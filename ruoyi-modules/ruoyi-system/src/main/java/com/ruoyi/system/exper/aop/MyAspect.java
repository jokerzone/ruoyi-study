package com.ruoyi.system.exper.aop;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class MyAspect {
    @Pointcut("execution(* com.ruoyi.system.exper.di.*.*(..))")  //表达式介绍请看后文
    // 切点固定无参无返回值,重点是声明需要增强的目标
    public void pt() {
    }

    @Before(value = "pt() ")//@Before前置通知,在目标方法执行之前运行
    public void before() {
        System.out.println("before执行了");
    }

    @After(value = "pt()")//@After前置通知,在目标方法执行之前运行
    public void after() {
        System.out.println("after执行了");
    }

}

