package com.ruoyi.system.exper.di;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class A {


     public A(){
         System.out.println("初始化A");
     }


    @Autowired
    private B b;


    public String name="A";

    public  void test(){

        System.out.println("输出A");
    }
}
