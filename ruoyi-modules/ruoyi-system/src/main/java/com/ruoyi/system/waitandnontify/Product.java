package com.ruoyi.system.waitandnontify;

public class Product {
    private static Integer count = 0;

    public static void add() {
        count++;
    }

    public static void delete() {
        count--;
    }

    public static Integer getCount(){
        return count;
    }
}
