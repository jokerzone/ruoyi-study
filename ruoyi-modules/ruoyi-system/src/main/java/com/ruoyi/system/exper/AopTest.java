package com.ruoyi.system.exper;

import com.ruoyi.system.RuoYiSystemApplication;
import com.ruoyi.system.exper.di.A;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//@SpringBootTest(classes = RuoYiSystemApplication.class)
//@RunWith(SpringRunner.class)
public class AopTest {
    @Autowired
    private A a;

    @Test
    public void test9() {
        a.test();
    }
}
