package com.ruoyi.system.service;

import com.ruoyi.system.domain.SysLog;
import com.ruoyi.system.domain.SysLogininfor;

import java.util.List;

/**
 * 博客信息的服务层
 */
public interface ISysLogService {

    /**
     * 新增博客
     *
     * @param sysLog 访问日志对象
     */
    public int insertLog(SysLog sysLog);

    /**
     * 查询博客系统
     *
     * @param pkid 博客对象的主键
     */
    public SysLog selectSysLog(Long pkid);


    /**
     * 更新博客系统
     *
     * @param sysLog 博客信息
     */
    public int updateSysLog(SysLog sysLog);


    /**
     * 获取指令条数的数据
     *
     * @param sysLog
     * @return
     */
    public List<SysLog> getList(SysLog sysLog);

    /**
     *  通过主键获取博文
     *
     * @param pkids
     * @return
     */
    public List<SysLog> getListByIds(Long[] pkids);
}
