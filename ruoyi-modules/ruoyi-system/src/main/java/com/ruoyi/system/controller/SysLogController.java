package com.ruoyi.system.controller;

import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.common.core.utils.SecurityUtils;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.PreAuthorize;
import com.ruoyi.system.domain.SysConfig;
import com.ruoyi.system.domain.SysLog;
import com.ruoyi.system.service.ISysConfigService;
import com.ruoyi.system.service.ISysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 参数配置 信息操作处理
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/log")
public class SysLogController extends BaseController {

    @Autowired
    private ISysLogService sysLogService;

    /**
     * 获取参数配置列表
     */

    @GetMapping("/{pkid}")
    public AjaxResult get(@PathVariable Long pkid) {
        SysLog sysLog = sysLogService.selectSysLog(pkid);
        return AjaxResult.success(sysLog);
    }


    @PostMapping("/update")
    public AjaxResult updateLog(@RequestBody SysLog sysLog) {
        return AjaxResult.success(sysLogService.updateSysLog(sysLog));
    }

    @PostMapping("/save")
    public AjaxResult insertLog(@RequestBody SysLog sysLog) {
        return AjaxResult.success(sysLogService.insertLog(sysLog));
    }

    @GetMapping("/list")
    public AjaxResult getList() {

        SysLog sysLog = new SysLog();

        List<SysLog> list = sysLogService.getList(sysLog);


        return AjaxResult.success(list);
    }

    @PostMapping("/listbyids")
    public AjaxResult getList(@RequestBody Long[] pkids) {
        List<SysLog> list = sysLogService.getListByIds(pkids);
        return AjaxResult.success(list);
    }

    @PostMapping("/param")
    public AjaxResult getLogByParam(@RequestBody ArrayList<String> list) {
        if (list.size() < 3) {
            AjaxResult.error("the params is  wrong");
        }
        int size = list.size();
        String section = list.get(size - 1);
        String domainblock = list.get(size - 2);
        String csdomain = list.get(size - 3);
        SysLog sysLog = new SysLog();
        sysLog.setCsdomain(csdomain);
        if (!section.equals(domainblock)){
            sysLog.setSection(section);
        }
        sysLog.setDomainblock(domainblock);
        List<SysLog> resultList = sysLogService.getList(sysLog);

        StringBuilder stringBuilder = new StringBuilder();
        for (SysLog log : resultList) {
            stringBuilder.append(log.getContent()+"\r\n");
        }
        return AjaxResult.success(resultList);
    }
}
