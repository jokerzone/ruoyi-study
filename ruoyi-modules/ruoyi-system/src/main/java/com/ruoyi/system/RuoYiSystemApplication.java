package com.ruoyi.system;

import com.ruoyi.system.exper.di.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.ruoyi.common.security.annotation.EnableCustomConfig;
import com.ruoyi.common.security.annotation.EnableRyFeignClients;
import com.ruoyi.common.swagger.annotation.EnableCustomSwagger2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * 系统模块
 *
 * @author ruoyi
 */
@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringBootApplication
public class RuoYiSystemApplication {


    public static void main(String[] args) {
//        SpringApplication.run(RuoYiSystemApplication.class, args);
//       System.out.println("(♥◠‿◠)ﾉﾞ  系统模块启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
//               " .-------.       ____     __        \n" +
//              " |  _ _   \\      \\   \\   /  /    \n" +
//             " | ( ' )  |       \\  _. /  '       \n" +
//                 " |(_ o _) /        _( )_ .'         \n" +
//                 " | (_,_).' __  ___(_ o _)'          \n" +
//                 " |  |\\ \\  |  ||   |(_,_)'         \n" +
//                 " |  | \\ `'   /|   `-'  /           \n" +
//                 " |  |  \\    /  \\      /           \n" +
//                 " ''-'   `'-'    `-..-'              ");
//
//        System.out.println(Math.abs( "consumer_demo1`".hashCode())%50);


        CopyOnWriteArrayList<String> copyOnWriteArrayList = new CopyOnWriteArrayList<>();

        copyOnWriteArrayList.add("first");
        copyOnWriteArrayList.add("second");

        Iterator<String> iterator = copyOnWriteArrayList.iterator();

        copyOnWriteArrayList.add("third");

        while (iterator.hasNext()) {
            System.out.println("第一次输出：" + iterator.next());
        }
        Iterator<String> iterator1 = copyOnWriteArrayList.iterator();

        while (iterator1.hasNext()) {
            System.out.println("第二次输出：" + iterator1.next());
        }


    }
}
