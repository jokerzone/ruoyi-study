package com.ruoyi.system.waitandnontify;

public class ThreadTest {

    static final Object obj = new Object();


    public static void main(String[] args) throws Exception {

        Thread consume = new Thread(new Consume(obj), "Consume");
        Thread produce = new Thread(new Produce(obj), "Produce");
        // 先启动消费者
        consume.setName("consume 线程");
        consume.start();
        produce.setName("produce 线程");
        produce.start();


    }


}
