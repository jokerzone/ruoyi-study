package com.ruoyi.system.spi;


import java.util.Arrays;
import java.util.Scanner;

public class Client {


    //  多个测试用例，每个测试用例一行。
    //每行通过,隔开，有n个字符，n＜100
    //对于每组用例输出一行排序后的字符串，用','隔开，无结尾空格


    //输入有两行，第一行n
    //第二行是n个空格隔开的字符串
    //输出一行排序后的字符串，空格隔开，无结尾空格


    //
    //多个测试用例，每个测试用例一行。
    //每行通过,隔开，有n个字符，n＜100
    //输出描述:
    //对于每组用例输出一行排序后的字符串，用','隔开，无结尾空格
    public static void test() {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            String s = scanner.nextLine();
            String[] split = s.split(",");
            Arrays.sort(split);
            for (int i = 0; i < split.length; i++) {
                if (i == split.length - 1) {
                    System.out.println(split[i]);
                }
                System.out.print(split[i] + ",");
            }

        }

    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String next = scanner.next();
        scanner.nextLine();
        if (scanner.hasNextLine()) {
            // 如果有一行数据，则获取
            String s = scanner.nextLine();
            String[] split = s.split(" ");
            Arrays.sort(split);
            int length = split.length;
            for (int i = 0; i < length; i++) {
                if (i == length - 1) {
                    System.out.println(split[i]);
                    continue;
                }
                System.out.print(split[i] + " ");
            }
        }


    }
}
