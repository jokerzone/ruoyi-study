package com.ruoyi.system.domain;

import com.ruoyi.common.core.annotation.Excel;

import java.io.Serializable;

public class SysLog  implements Serializable {
    /**
     * 主键
     */
    @Excel(name = "主键", cellType = Excel.ColumnType.NUMERIC)
    private Long pkid;

    //  知识领域
    private String csdomain;

    // 内容
    private String content;

    // 领域模块
    private String domainblock;
    // 小节
    private String cssection;

    // 创建人
    private String creator;



    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getPkid() {
        return pkid;
    }

    public void setPkid(Long pkid) {
        this.pkid = pkid;
    }

    public String getCsdomain() {
        return csdomain;
    }

    public void setCsdomain(String csdomain) {
        this.csdomain = csdomain;
    }

    public String getDomainblock() {
        return domainblock;
    }

    public void setDomainblock(String domainblock) {
        this.domainblock = domainblock;
    }

    public String getSection() {
        return cssection;
    }

    public void setSection(String cssection) {
        this.cssection = cssection;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }
}
