package com.ruoyi.system.waitandnontify;

import java.util.concurrent.TimeUnit;

public class Produce implements Runnable {

    private Object object;

    public Produce(Object object) {
        this.object = object;
    }

    @Override
    public void run() {
        synchronized (object) {
            System.out.println("++++ 进入生产者线程");
            System.out.println("++++ 产品数量:" + Product.getCount());
            while (true) {
                try {
                    TimeUnit.MILLISECONDS.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (Product.getCount() <= 0) {
                    System.out.println("++++ 开始生产!");
                    Product.add();
                    System.out.println("++++ 生产后产品数量:" + Product.getCount());
                }else {
                    try {
                        // 通知消费者进行消费，自己进入等待
                        object.notify();
                        object.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }

        }
    }
}