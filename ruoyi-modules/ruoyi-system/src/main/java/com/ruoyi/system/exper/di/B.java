package com.ruoyi.system.exper.di;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class B {

    public B(){
        System.out.println("初始化B");
    }

    @Autowired
    private A a;


    public String name="b";

    public  void test(){

        System.out.println("输出B");
    }
}
