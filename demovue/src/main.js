// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Element from 'element-ui'

import {mavonEditor} from 'mavon-editor'

import axios from 'axios'

import 'mavon-editor/dist/css/index.css'
import VueMarkdown from 'vue-markdown'



Vue.prototype.$axios = axios
Vue.config.productionTip = false
Vue.use(Element)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App,VueMarkdown ,axios,mavonEditor},
  template: '<App/>'
})
