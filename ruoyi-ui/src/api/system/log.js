import request from '@/utils/request'


// 查询用户列表
export function listUser(pkid) {
  return request({
    url: '/system/log/'+pkid,
    method: 'get',
  })
}


// 修改用户
export function updateLog(data) {
    return request({
      url: '/system/log/update',
      method: 'post',
      data: data
    })
  }
  

  // 修改用户
export function insertLog(data) {
    return request({
      url: '/system/log/save',
      method: 'post',
      data: data
    })
  }